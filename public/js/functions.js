(function( $ ) {
    $(document).ready(function (){
        let mainNavigation= document.getElementsByClassName("main-navigation");
        let menutoggle = document.getElementsByClassName("menu-toggle");
        let linkItems = document.getElementsByClassName("link-item");

        // Menu de navegacion
        $(document).on('click','.menu-toggle',function(event){
            event.preventDefault();
            // Cuando el menu se cierra
            if(mainNavigation[0].classList.contains("open")){
                menutoggle[0].innerHTML =
                '<img src="./assets/shared/icon-hamburger.svg" alt="close menu button" />';
                mainNavigation[0].classList.remove("open");
            } // Cuando abrimos el menu
            else {
                menutoggle[0].innerHTML =
                '<img src="./assets/shared/icon-close.svg" alt="close menu button" />';
                mainNavigation[0].style.display="grid";
                mainNavigation[0].className += " open";
            }
        });

        $(document).on('click','.link-item',function(event){
            event.preventDefault();
            const item = document.getElementById(event.currentTarget.id)
    
            if(item.classList.contains("active")){
                return;
            }

            removeActiveClass(linkItems);
            item.className += " active";
        });

        function removeActiveClass(items){
            for (let item of items) {
                if(item.classList.contains("active")){
                    item.classList.remove("active");
                }
            };
        }

        // Carrousel Destination Page
        var isDestinationPage = document.getElementById('destination-page');
        if(isDestinationPage) showDestinationDivs(0);

        function showDestinationDivs(n) {

            // imagenes
            var thumbnails = document.getElementsByClassName("entry-thumbnail");
            for (i = 0; i < thumbnails.length; i++) {
                thumbnails[i].style.display = "none";
            }
            thumbnails[n].style.display = "grid";

            // descripcion
            var summaries = document.getElementsByClassName("entry-description");
            for (i = 0; i < summaries.length; i++) {
                summaries[i].style.display = "none";
            }
            summaries[n].style.display = "grid";

            //propiedades
            var distances = document.getElementsByClassName("entry-distance");
            var times = document.getElementsByClassName("entry-time");
            for (i = 0; i < distances.length; i++) {
                distances[i].style.display = "none";
                times[i].style.display = "none";
            }
            distances[n].style.display = "grid";
            times[n].style.display = "grid";
        }

        $(document).on('click','.link-item',function(event){
            event.preventDefault();
            const idLink = event.currentTarget.id;
            if(idLink.includes('moon')){
                showDestinationDivs(0);
            } else if(idLink.includes('mars')){
                showDestinationDivs(1);
            } else if(idLink.includes('europa')){
                showDestinationDivs(2);
            } else if(idLink.includes('titan')){
                showDestinationDivs(3);
            }
        });

        // Carrousel Crew Page
        var isCrewPage = document.getElementById('crew-page');
        if(isCrewPage) showCrewDivs(0);

        function showCrewDivs(n) {
 
            // imagenes
            var thumbnails = document.getElementsByClassName("entry-thumbnail");
            for (i = 0; i < thumbnails.length; i++) {
                thumbnails[i].style.display = "none";
            }
            thumbnails[n].style.display = "grid";

            // descripcion
            var biographies = document.getElementsByClassName("entry-biography");
            for (i = 0; i < biographies.length; i++) {
            biographies[i].style.display = "none";
            }
            biographies[n].style.display = "grid";
        }

        $(document).on('click','.link-item',function(event){
            event.preventDefault();
            const idLink = event.currentTarget.id;
            if(idLink.includes('commander')){
            showCrewDivs(0);
            } else if(idLink.includes('specialist')){
            showCrewDivs(1);
            } else if(idLink.includes('pilot')){
            showCrewDivs(2);
            } else if(idLink.includes('engineer')){
            showCrewDivs(3);
            }
        });

        // Carrousel Technology Page
        var isTechnologyPage = document.getElementById('technology-page');
        if(isTechnologyPage) showTechnologyDivs(0);

        function showTechnologyDivs(n) {
 
            // imagenes
            var thumbnails = document.getElementsByClassName("entry-thumbnail");
            for (i = 0; i < thumbnails.length; i++) {
                thumbnails[i].style.display = "none";
            }
            thumbnails[n].style.display = "grid";

            // descripcion
            var biographies = document.getElementsByClassName("entry-description");
            for (i = 0; i < biographies.length; i++) {
            biographies[i].style.display = "none";
            }
            biographies[n].style.display = "grid";
        }

        $(document).on('click','.link-item',function(event){
            event.preventDefault();
            const idLink = event.currentTarget.id;
            if(idLink.includes('vehicle')){
                showTechnologyDivs(0);
            } else if(idLink.includes('spaceport')){
                showTechnologyDivs(1);
            } else if(idLink.includes('capsule')){
                showTechnologyDivs(2);
            }
        });
    })
})( jQuery );